﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        private static object Locker = new object();
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            lock (Locker)
            {
                return Task.FromResult(Data);
            }
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            lock (Locker)
            {
                return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
            }
        }

        public Task CreateAsync(T entity)
        {            
            var data = Data as List<T>;
            lock (Locker)
            {
                data.Add(entity);
            }
            Data = data;
            entity.Id = Guid.NewGuid();
            return Task.CompletedTask;
        }

        public Task UpdateAsync(T entity)
        {     
            var data = Data as List<T>;
            lock (Locker)
            {
                var fromDb = data.FirstOrDefault(x => x.Id == entity.Id);
                data.Remove(fromDb);
                data.Add(entity);
            }
            Data = data;
            return Task.CompletedTask;
        }

        public Task DeleteAsync(T entity)
        {
            var data = Data as List<T>;
            lock (Locker)
            {
                var fromDb = data.FirstOrDefault(x => x.Id == entity.Id);
                data.Remove(fromDb);
            }
            Data = data;
            return Task.CompletedTask;
        }

        public Task<IEnumerable<T>> GetByCondition(Func<T, bool> predicate)
        {
            var data = Data as List<T>;
            return Task.FromResult(data.Where(predicate).AsEnumerable());
        }
    }
}